<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'sku' => 'MILK2L',
            'price' => 2.39,
            'title' => '2L Milk'
        ]);

        Product::create([
            'sku' => 'MILK1L',
            'price' => 1.39,
            'title' => '1L Milk'
        ]);

        Product::create([
            'sku' => 'BREAD',
            'price' => 1.39,
            'title' => 'White'
        ]);

        Product::create([
            'sku' => 'BROWNBREAD',
            'price' => 2.39,
            'title' => 'Brown Bread'
        ]);
    }
}

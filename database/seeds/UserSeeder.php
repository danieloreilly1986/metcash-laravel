<?php

use App\Order;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i = 1; $i <= 50; $i++) {

            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->safeEmail;
            $user->password = \Illuminate\Support\Facades\Hash::make('password123');
            $user->save();

            Order::create([
                'user_id' => $user->id,
                'amount' => $faker->numberBetween(20, 300),
            ]);
        }
    }
}

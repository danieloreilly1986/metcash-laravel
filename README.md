# **Installation**

1. Clone the repository.
2. Install composer packages ```composer install```
3. Update .env file with database connection details and url.
4. Run migrations ```php artisan db:migrate```
4. Run seeders (optional to fill dummy data) ```php artisan db:seed```

# **API Documentation**
https://documenter.getpostman.com/view/1380307/TVCfUSYx

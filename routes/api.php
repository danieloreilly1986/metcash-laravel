<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('user', 'UserController@getUsers');
Route::post('user', 'UserController@createUser');
Route::put('user/{id}', 'UserController@updateUser');
Route::delete('user/delete', 'UserController@deleteUsers');

Route::get('user/{id}/order', 'UserOrderController@getOrders');

Route::get('product', 'ProductController@getProducts');
Route::post('product', 'ProductController@createProduct');
Route::put('product/{id}', 'ProductController@updateProduct');
Route::delete('product/delete', 'ProductController@deleteProducts');

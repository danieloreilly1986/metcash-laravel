<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;

class UserOrderController extends Controller
{
    public function getOrders($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return response(['message' => 'User not found.'], 404);
        }

        return $user->orders;
    }
}

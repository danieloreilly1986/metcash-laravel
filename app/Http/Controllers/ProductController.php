<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\DeleteProductsRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;

class ProductController extends Controller
{
    public function getProducts()
    {
        return Product::all();
    }

    public function createProduct(CreateProductRequest $request)
    {
        $validated = $request->validated();

        return Product::create($validated);
    }

    public function updateProduct($productId, UpdateProductRequest $request)
    {
        $validated = $request->validated();

        $product = Product::find($productId);

        if (!$product) {
            return response(['message' => 'Product not found.'], 404);
        }

        $product->fill($validated);
        $product->save();

        return $product;
    }

    public function deleteProducts(DeleteProductsRequest $request)
    {
        $validated = $request->validated();

        $product = Product::whereIn('id', $validated['productIds'])->delete();

        return response(['message' => 'Products deleted.'], 202);
    }
}

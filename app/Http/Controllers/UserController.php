<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\DeleteUsersRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUsers()
    {
        return User::all();
    }

    public function createUser(CreateUserRequest $request)
    {
        $validated = $request->validated();

        $validated['password'] = \Illuminate\Support\Facades\Hash::make($validated['password']);

        return User::create($validated);
    }

    public function updateUser($userId, UpdateUserRequest $request)
    {
        $validated = $request->validated();

        if (isset($validated['password'])) {
            $validated['password'] = \Illuminate\Support\Facades\Hash::make($validated['password']);
        }

        $user = User::find($userId);

        if (!$user) {
            return response(['message' => 'User not found.'], 404);
        }

        $user->fill($validated);
        $user->save();

        return $user;
    }

    public function deleteUsers(DeleteUsersRequest $request)
    {
        $validated = $request->validated();

        $user = User::whereIn('id', $validated['userIds'])->delete();

        return response(['message' => 'Users deleted.'], 202);
    }
}
